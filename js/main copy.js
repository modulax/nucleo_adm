$(document).ready(function()
{

    var icons = {}, icon = [];
    var folder = "mini";


    $('#folderChoose').on('change', function()
    {
        folder = this.value;
        $('#form').submit();
    })




       $('#form').submit(function(e)
       {
            e.preventDefault();
            var breaks = "";
            var iconItem = "";
            icons = {};
            icon = [];

                $.ajax({
                url : "icons/" + folder + "/tags.txt",
                dataType: "text",
                success : function (data)
                {

                    breaks = data.split('\n');
                    for(var i = 0; i < breaks.length; i ++)
                    {

                      iconItem = breaks[i].split(/([^=:])=(?!=)/g);
                      iconItem[2] = iconItem[2].replace(/\s/g, '');

                      // ADD TO ARRAY
                      if(iconItem[4])
                      {
                        iconItem[4] = iconItem[4].replace(/\s/g, '');
                      }
                      icon.push({name: iconItem[0], folders: iconItem[2], tags: iconItem[4]});

                    }
                    icons ={icon};
                    console.log(icons);

                    searchIcons();

                    }
                });



       });


       /************* SEARCH *************/
       function searchIcons()
       {
            var query = $("#query").val().toLowerCase();

            $("#content").html('');

            if(folder == "glyph" || folder == "outline")
            {
               var newfolder = folder + '/64px';
            }else
            {
              var newfolder = folder;
            }

            var results = {};


            for(var i = 0; i < icon.length; i ++)
            {
                if(icons.icon[i].tags)
                {
                        var myTags = icons.icon[i].tags;
                        var splitTags = myTags.split(",");
                        var hitCount = 0;

                        for (var j = 0; j < splitTags.length; j++)
                        {

                            // if(splitTags[j] == query)
                            var re = new RegExp(query, 'g');

                            if(splitTags[j].match(re))
                            {
                                ++hitCount;
                                if(hitCount <=1)
                                {
                                    var myFolder =  icons.icon[i].folders.split(",");
                                    var imagePath = 'icons/' + newfolder + '/' + myFolder[0] + '/' + icons.icon[i].name + '.svg';
                                    // $("#content").append('<img src="icons/' + newfolder + '/' + myFolder[0] + '/' + icons.icon[i].name + '.svg" onerror="this.remove()" class="icon">')
                                    getImage(imagePath)
                                }


                            }

                        }
                }

            }


       }

      function getImage(name)
      {
          // var image_url = "images/" + name + ".jpg";

          $.get(name)
              .done(function()
              {
                  // return image_url;
                  $("#content").append('<div class="iconWrap"><img src="' + name + '"class="icon"></div>');
              })
              .fail(function()
              {
                  // return "images/default.jpg";
              });

      }






});