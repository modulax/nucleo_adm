$(document).ready(function()
{

    var icons = {}, icon = [];
    var folder = "outline";


    $('#folderChoose').on('change', function()
    {
        // folder = this.value;
        // $('#form').submit();
    })



      icons = {};
      icon = [];

       $('#form').submit(function(e)
       {
            e.preventDefault();
            var breaks = "";
            var iconItem = "";


                $.ajax({
                url : "icons/" + folder + "/tags.txt",
                dataType: "text",
                success : function (data)
                {

                    breaks = data.split('\n');
                    for(var i = 0; i < breaks.length; i ++)
                    {

                      iconItem = breaks[i].split(/([^=:])=(?!=)/g);
                      iconItem[2] = iconItem[2].replace(/\s/g, '');

                      iconItem[2] = iconItem[2].split(",");
                      if(iconItem[2].length >= 2)
                      {
                        iconItem[2] = iconItem[2][0];
                      }

                      // ADD TO ARRAY
                      if(iconItem[4])
                      {
                        iconItem[4] = iconItem[4].replace(/\s/g, '');
                      }



                      var path = 'icons/outline/64px/' + iconItem[2] + '/' + iconItem[0] + '.svg';
                      cleanIcons(path, iconItem[0], iconItem[2],iconItem[4]);


                    }
                    icons.icons =  icon ;
                    console.log(icon);

                    // searchIcons();

                    }
                });



       });



       function cleanIcons(path, name, folder, tags)
       {
          $.get(path)
              .done(function()
              {
                  icon.push({name: name, folders: folder, tags: tags});
              })
              .fail(function()
              {
                return false;
                  // return "images/default.jpg";
              });
       }


       /************* SEARCH *************/
       function searchIcons()
       {
            var query = $("#query").val().toLowerCase();

            $("#content").html('');

            if(folder == "glyph" || folder == "outline")
            {
               var newfolder = folder + '/64px';
            }else
            {
              var newfolder = folder;
            }

            var results = {};


            for(var i = 0; i < icon.length; i ++)
            {
                if(icons.icon[i].tags)
                {
                        var myTags = icons.icon[i].tags;
                        var splitTags = myTags.split(",");
                        var hitCount = 0;

                        for (var j = 0; j < splitTags.length; j++)
                        {

                            // if(splitTags[j] == query)
                            var re = new RegExp(query, 'g');

                            if(splitTags[j].match(re))
                            {
                                ++hitCount;
                                if(hitCount <=1)
                                {
                                    var myFolder =  icons.icon[i].folders.split(",");
                                    var imagePath = 'icons/' + newfolder + '/' + myFolder[0] + '/' + icons.icon[i].name + '.svg';
                                    // $("#content").append('<img src="icons/' + newfolder + '/' + myFolder[0] + '/' + icons.icon[i].name + '.svg" onerror="this.remove()" class="icon">')
                                    getImage(imagePath)
                                }


                            }

                        }
                }

            }


       }

      function getImage(name)
      {
          // var image_url = "images/" + name + ".jpg";

          $.get(name)
              .done(function()
              {
                  // return image_url;
                  $("#content").append('<div class="iconWrap"><img src="' + name + '"class="icon"></div>');
              })
              .fail(function()
              {
                  // return "images/default.jpg";
              });

      }






});